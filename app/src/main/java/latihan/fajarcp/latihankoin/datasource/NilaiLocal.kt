package latihan.fajarcp.latihankoin.datasource

import android.arch.lifecycle.LiveData
import android.content.Context
import latihan.fajarcp.latihankoin.db.MyDatabase
import latihan.fajarcp.latihankoin.model.Nilai

class NilaiLocal(val context: Context) {

    fun getAll() : LiveData<List<Nilai>> {
        return MyDatabase.getInstance(context)?.nilaiDao()!!.getAll()
    }

    fun insert(nilai: Nilai) {
        MyDatabase.getInstance(context)?.nilaiDao()!!.insert(nilai)
    }

    fun update(nilai: Nilai) {
        MyDatabase.getInstance(context)?.nilaiDao()!!.update(nilai)
    }

    fun delete(nilai: Nilai) {
        MyDatabase.getInstance(context)?.nilaiDao()!!.delete(nilai)
    }

}