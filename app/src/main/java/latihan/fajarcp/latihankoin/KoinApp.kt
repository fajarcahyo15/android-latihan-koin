package latihan.fajarcp.latihankoin

import android.app.Application
import latihan.fajarcp.latihankoin.module.KoinModule
import org.koin.android.ext.android.startKoin

class KoinApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(KoinModule().get()))
    }
}