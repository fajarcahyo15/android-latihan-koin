package latihan.fajarcp.latihankoin.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.rvlist_nilai.view.*
import latihan.fajarcp.latihankoin.R
import latihan.fajarcp.latihankoin.model.Nilai

class NilaiAdapter : RecyclerView.Adapter<NilaiAdapter.NilaiViewHolder>() {

    private var listNilai : MutableList<Nilai> = mutableListOf()

    private var onLoad: OnComponentLoad? = null

    interface OnComponentLoad {
        fun onItemLoad(nilai: Nilai, holder: NilaiViewHolder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NilaiViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rvlist_nilai, parent, false)
        return NilaiViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listNilai.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: NilaiViewHolder, position: Int) {
        holder.tv_nama.text = " : ${listNilai.get(position).nama}"
        holder.tv_nilai.text = " : ${listNilai.get(position).nilai}"

        onLoad?.onItemLoad(listNilai.get(position), holder)
    }

    class NilaiViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val tv_nama = v.tv_nama
        val tv_nilai = v.tv_nilai
    }

    fun add(nilai: Nilai) {
        this.listNilai.add(nilai)
        notifyDataSetChanged()
    }

    fun addAll(listNilai : List<Nilai>) {
        this.listNilai.addAll(listNilai)
        notifyDataSetChanged()
    }

    fun update(listNilai: List<Nilai>) {
        this.listNilai = listNilai as MutableList<Nilai>
        notifyDataSetChanged()
    }

    fun setOnLoad(onLoad: OnComponentLoad) {
        this.onLoad = onLoad
    }
}