package latihan.fajarcp.latihankoin.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import latihan.fajarcp.latihankoin.db.dao.NilaiDao
import latihan.fajarcp.latihankoin.model.Nilai

@Database(entities = arrayOf(
    Nilai::class
), version = 1, exportSchema = false)
abstract class MyDatabase : RoomDatabase() {

    abstract fun nilaiDao(): NilaiDao

    companion object {
        private var INSTANCE: MyDatabase? = null

        fun getInstance(context: Context): MyDatabase? {
            if (INSTANCE == null) {
                synchronized(MyDatabase::class) {
                    INSTANCE = Room.databaseBuilder<MyDatabase>(context.applicationContext,
                        MyDatabase::class.java, "mhs.db")
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}