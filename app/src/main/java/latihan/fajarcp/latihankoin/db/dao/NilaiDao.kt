package latihan.fajarcp.latihankoin.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import latihan.fajarcp.latihankoin.model.Nilai

@Dao
interface NilaiDao {

    @Query("SELECT * FROM nilai")
    fun getAll(): LiveData<List<Nilai>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(nilai: Nilai)

    @Update
    fun update(nilai: Nilai)

    @Delete
    fun delete(nilai: Nilai)
}