package latihan.fajarcp.latihankoin.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "nilai")
data class Nilai(
    @PrimaryKey val id: String,
    val nama: String,
    val nilai: Int
)