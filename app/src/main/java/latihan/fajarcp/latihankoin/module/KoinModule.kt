package latihan.fajarcp.latihankoin.module


import latihan.fajarcp.latihankoin.adapter.NilaiAdapter
import latihan.fajarcp.latihankoin.datasource.NilaiLocal
import latihan.fajarcp.latihankoin.view_model.MainViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

class KoinModule {

    fun get() : Module = module {
        single { NilaiLocal(get()) }
        single { NilaiAdapter() }
        viewModel { MainViewModel(get()) }
    }
}