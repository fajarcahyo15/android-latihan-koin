package latihan.fajarcp.latihankoin.view_model

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import latihan.fajarcp.latihankoin.datasource.NilaiLocal
import latihan.fajarcp.latihankoin.model.Nilai
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

class MainViewModel(val nilaiLocal: NilaiLocal) : ViewModel() {

    fun insert(nama: String, nilai: Int) {
        doAsync {
            nilaiLocal.insert(Nilai(UUID.randomUUID().toString(), nama, nilai))
        }
    }

    fun delete(nilai: Nilai): MutableLiveData<Boolean> {
        val sukses = MutableLiveData<Boolean>()
        sukses.value = false
        doAsync {
            nilaiLocal.delete(nilai)
            uiThread {
                sukses.value = true
            }
        }
        return sukses
    }

}