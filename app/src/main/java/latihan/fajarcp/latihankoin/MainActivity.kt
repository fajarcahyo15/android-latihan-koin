package latihan.fajarcp.latihankoin

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import latihan.fajarcp.latihankoin.adapter.NilaiAdapter
import latihan.fajarcp.latihankoin.datasource.NilaiLocal
import latihan.fajarcp.latihankoin.model.Nilai
import latihan.fajarcp.latihankoin.view_model.MainViewModel
import org.jetbrains.anko.longToast
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    val mainViewModel : MainViewModel by viewModel()
    val nilaiAdapter : NilaiAdapter by inject()
    val nilaiLocal : NilaiLocal by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_nilai.layoutManager = LinearLayoutManager(this)
        rv_nilai.setHasFixedSize(true)
        rv_nilai.itemAnimator = DefaultItemAnimator()
        rv_nilai.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rv_nilai.adapter = nilaiAdapter

        nilaiLocal.getAll().observe(this, Observer {
            if (it != null) nilaiAdapter.update(it)
        })

        btn_tambah.setOnClickListener {
            val nama = txt_nama.text.toString()
            val nilai = txt_nilai.text.toString().toInt()
            mainViewModel.insert(nama, nilai)

            txt_nama.setText("")
            txt_nilai.setText("")
        }

        nilaiAdapter.setOnLoad(object : NilaiAdapter.OnComponentLoad {
            override fun onItemLoad(nilai: Nilai, holder: NilaiAdapter.NilaiViewHolder) {
                holder.itemView.setOnClickListener {
                    mainViewModel.delete(nilai).observe(this@MainActivity, Observer { sukses->
                        if ((sukses != null) && sukses) longToast("Nilai ${nilai.nama} telah dihapus.")
                    })
                }
            }
        })
    }
}
